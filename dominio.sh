#########################################################################################
# Purpose : Insert VM CentOS Domain Microsoft                                           #
# Descrição: insserir centos 8 no ADDS                                                  #
# Version: 1.0                                                                          #
# Author  : George Rodrigues de Oliveira - georgeroliveira@gmail.com                    #
# Release Date: 14/01/2020                                                              # 
#########################################################################################

#[instalar os pacotes necessários para ingresso da vm ao Dominio]
yum install realmd sssd oddjob oddjob-mkhomedir adcli samba-common samba-common-tools krb5-workstation -y

#[inserir a vm ao Dominio]
realm join dominio.local --user="USUSARIO DOMINIO"                                       

#[Para não exibir o domínio@usuário altere o seguinte parâmetro]
sed -i 's/use_fully_qualified_names = True/use_fully_qualified_names = False/'  /etc/sssd/sssd.conf

systemctl restart sssd

#[inserir grupo espcecifico do ADDS para ter acesso root]
sed -i '100 a %grupo\\ do\\ dominio ALL=(ALL) ALL' /etc/sudoers

#[Permitir grupo especifico para ter acesso ao root]
realm permit -g grupo\ do\ dominio@dominio.local

service sshd restart