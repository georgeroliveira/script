#########################################################################################
# Purpose :  vmware initial configuration centOS 8                                      #
# Descrição: Configuração Padrão                                                        #
# Version: 1.0                                                                          #
# Author  : George Rodrigues de Oliveira - georgeroliveira@gmail.com                    #
#########################################################################################

#[Alterar nome da VM]
hostnamectl set-hostname "NOME DO HOST"

#[Alterar nome da interface de rede no ambiente VMware de ens192 para eth0 centOS]
sed -i 's/quiet"/quiet net.ifnames=0 biosdevname=0"/' /etc/default/grub
grub2-mkconfig -o /boot/grub2/grub.cfg

#[Configuração de placa de rede]
mv /etc/sysconfig/network-scripts/ifcfg-ens192 /etc/sysconfig/network-scripts/ifcfg-eth0
sed -i 's/NAME=ens192/NAME=eth0/' /etc/sysconfig/network-scripts/ifcfg-eth0
sed -i 's/DEVICE=ens192/DEVICE=eth0/' /etc/sysconfig/net

#[Restart/Status interfce de rede]
systemctl restart NetworkManager.service
systemctl status  NetworkManager.service

#[Disable ipv6]
vim /etc/sysctl.conf

sed -i '10 a net.ipv6.conf.all.disable_ipv6 = 1 ' /etc/sysctl.conf
sed -i '11 a net.ipv6.conf.all.disable_ipv6 = 1 ' /etc/sysctl.conf
sysctl -p

#[Configurar fuso horário]
tzselect 

#[Setar timezone manual]
timedatectl set-timezone "TIMEZONE DA SUA REGIÃO"
timedatectl

#[Install DNF gerenciador de pacotes]
yum install dnf -y

#[update da VM]
dnf -y update

#[atualiza/instala o perl pacote para instalar o vmware-tools]
dnf -y install perl

#[install/configure NTP ]
dnf install chrony -y
systemctl start chronyd
systemctl enable chronyd
systemctl enable --now chronyd

sed -i '3d' /etc/chrony.conf
sed -i '2 a server "SERVER" iburst '  /etc/chrony.conf
sed -i '3 a server "SERVER" iburst '  /etc/chrony.conf
sed -i '4 a server "SERVER" iburst '  /etc/chrony.conf
systemctl restart chronyd
chronyc sources
chronyc tracking

#[remove open-vm-tools ]
dnf remove open-vm-tools -y 

#[Install Vmtools]
mkdir /mnt/cdrom
mount /dev/cdrom /mnt/cdrom
cd /mnt/cdrom
cp VMwareTools* /tmp
cd /tmp/
tar -xvzf VMwareTools*
cd vmware-tools-distrib/
chmod +x vmware-install.pl
./vmware-install.pl 

#[Deletar os logs das VMS criadas para ser entregue a terceiros ] 
>/var/log/lastlog && >/var/log/wtmp && >/var/log/btmp
history -c && history -w && exit





