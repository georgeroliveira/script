#[alterar layout teclado ubuntu ]
setxkbmap -model abnt2 -layout br -variant abnt2

#[windows Update quando não atualizar direto.]
wuauclt /detectnow /reportnow /updatenow
Windows8.1-KB3102812-x64


#[elastic search]
filebit + logstash

#[sed para subistituir "" por '']
sed -i 's/\"/d' '/\"/d' [subtistuir duas]

#[adicionar porta especifica no firewall da VM]
firewall-cmd --permanent --add-port=12201/tcp
firewall-cmd --reload
firewall-cmd --list-all


#[sed para adicionar/subistituir ]

sed -i '4 a SUBSYSTEM=="net",SYSFS{address}=="00:50:56:b9:6b:65",NAME="eth4"' /etc/udev/rules.d/z99-network.rules

sed -i 's/SUBSYSTEM=="net",SYSFS{address}=="00:50:56:b9:29:b1",NAME="eth4"/SUBSYSTEM=="net",
SYSFS{address}=="00:50:56:b9:87:97",NAME="eth4"/'  /etc/udev/rules.d/z99-network.rules

#[checar saudo do cluster do elastic]
curl -X GET "http://172.16.9.120:9200/_cluster/health?pretty=true"

#[sed para alterar placa de rede]
sed -i 's/BOOTPROTO=nome/BOOTPROTO=static/'  /etc/sysconfig/network-scripts/ifcfg-eth0

# echo; type; PATH
# cat; head; tail; sort; less; wc
# uniq; od; paste; split
# tr; cut; sed
# xzcat; bzcat; zcat
# checksums
# cd; ls; file
# touch, cp, mv


#[Capturar log dos comandos digitados]
#Passo 1:
#Use seu editor de texto favorito para abrir o / etc / bashrc e anexar a seguinte linha no final:
export PROMPT_COMMAND='RETRN_VAL=$?;logger -p local6.debug "$(whoami) [$$]: $(history 1 | sed "s/^[ ]*[0-9]\+[ ]*//" ) [$RETRN_VAL]"'

#Etapa 2:
#configure o syslogger para interceptar local6 em um arquivo de log adicionando esta linha no arquivo /etc/syslog.conf:
local6.*                /var/log/cmdlog.log

#Etapa 3:
service syslog restart

yum list installed mysql\*
yum list installed zabbix\*
rpm -qa | grep mysql
rpm -qa | grep zabbix

#[quando não atualizar]
yum clean all

#[checar update Vcenter]
tdnf upgrade tdnf --refresh
tdnf upgrade rsyslog.x86_64
systemctl status rsyslog.service
systemctl restart rsyslog.service
systemctl status rsyslog.service

#[desinstalar mongog]
$https://stackoverflow.com/questions/8766579/uninstalling-mongo

#[instalar mongog]
https://docs.mongodb.com/manual/tutorial/install-mongodb-on-red-hat/